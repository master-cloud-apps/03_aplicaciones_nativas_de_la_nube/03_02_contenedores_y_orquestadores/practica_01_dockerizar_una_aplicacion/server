const { expect } = require('chai')
const { manageInMemoryDatabase } = require('./../index.js')
const Task = require('./../../src/models/task.js')

describe('Task model', () => {
  manageInMemoryDatabase()
  it('When create Task, it should be foundable in the database', () => {
    const createTask = () => {
      return new Task({
        completed: false,
        text: 'This a test task',
        progress: 0,
        id: 1
      })
    }
    const task = createTask()
    return task.save()
      .then(newTask => {
        expect(newTask.id).to.not.be.equal(undefined)
        expect(newTask.completed).to.be.equal(false)
        expect(newTask.id).to.be.equal(1)
      })
  })
})
