const { connect, clearDatabase, closeDatabase } = require('./db-handler.js')

module.exports.manageInMemoryDatabase = () => {
  before(() => connect())

  afterEach(() => clearDatabase())

  after(() => closeDatabase())
}
