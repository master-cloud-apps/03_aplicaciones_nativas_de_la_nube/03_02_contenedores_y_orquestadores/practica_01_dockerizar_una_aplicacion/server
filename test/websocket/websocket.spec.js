const { expect } = require('chai')
const { sendMessageToSavedWebSocket, createWebsocket } = require('../../src/websocket')

describe('Testing websocket module', () => {
  it('Given no createWebsocket, when sendMessageToSavedWebSocket should not send anything', () => {
    sendMessageToSavedWebSocket({ text: 'Test message' })
  })

  it('Given socket created, when sendMessageToSavedWebSocket should send real message', () => {
    const testApp = {
      ws (websocketUri, callback) {
        callback(this.testWebsocket, null)
      },
      testWebsocket: {
        requestNumber: 0,
        send () {
          return this.requestNumber++
        }
      }
    }
    createWebsocket({ app: testApp })

    sendMessageToSavedWebSocket({ text: 'Test Message' })

    expect(testApp.testWebsocket.requestNumber).to.be.equal(1)
  })
})
