const { expect } = require('chai')
const { manageInMemoryDatabase } = require('..')
const { processTaskProgressMessage } = require('../../src/queue')
const Task = require('./../../src/models/task.js')

describe('consumeTask tests', () => {
  manageInMemoryDatabase()
  it('When consumeTask should update element', () => {
    return new Task({
      id: 1,
      progress: 0,
      completed: false
    }).save().then(() => {
      console.log('Saved')
      const msg = {
        content: {
          toString: () => JSON.stringify({ id: 1, progress: 100 })
        }
      }
      return processTaskProgressMessage(msg)
    })
      .then(() => Task.find({ id: 1 }))
      .then(tasks => expect(tasks[0].progress).to.be.equal(100))
  })
})
