const { expect } = require('chai')
const { processTask } = require('../../src/queue')

describe('Queue tests', () => {
  let task = {}
  let finalValue = {}
  beforeEach(() => {
    task = {}
    finalValue = {}
  })
  const amqpChannel = {
    value: { id: 0 },
    modifyValue () {
      this.value.id++
      return this.value
    },
    sendToQueue () {
      finalValue.id = this.modifyValue().id
    }
  }
  it('When processTask should send a message to the queue', () => {
    expect(finalValue.id).to.be.equal(undefined)

    processTask({ amqpChannel, task })

    expect(finalValue.id).to.be.equal(1)
  })
  it('Given null channel, When processTask nothing happens', () => {
    processTask({ task })

    expect(finalValue.id).to.be.equal(undefined)
  })
})
