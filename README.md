# Server

![Application](/docs/practica-dockerizar.png)

## Instrucciones de la práctica para el Server

### Ampliación

* Se deberá conectar este servicio con una Base de Datos Mongo, de forma que cada vez que se reciba
una petición, deberá registrarse en una colección de la Base de Datos. El formato concreto queda a criterio
del alumno.

### Empaquetado con Docker

Se debe empaquetar la aplicación siguiendo estos criterios:
* Cada servicio deberá empaquetarse en su propio contenedor.
* Para las bases de datos Mongo y MySQL, y para el servicio RabbitMQ, se usarán
los contenedores publicados en DockerHub.
* Para los servicios Server, Worker y ExternalService, habrá que crear contenedores
propios que deberán publicarse en una cuenta de DockerHub del alumno.
* Todos los contenedores de la aplicación estarán coordinados usando dockercompose.
* Se deberán un docker-compose de ejecución que usará las imágenes publicadas en
DockerHub.
* Creación de contenedores:
  * El servicio Server se empaquetará en Docker usando Dockerfile.
  * El servicio ExternalService se empaquetará usando buildpack.
  * Para el servicio Worker se deberán crear dos imágenes Docker diferentes con
diferentes estrategias:
    * Multistage dockerfile: Hay que usar el cacheo de las librerías Maven.
    * JIB
  * Se deberá probar en el docker-compose que ambas imágenes funcionan
correctamente.
  * El servicio Worker deberá esperar a que la base de datos MySQL esté disponible
usando código Java en el propio servicio.
  * El servicio ExternalService deberá esperar a que RabbitMQ está disponible
usando wait-for-it.sh
* Desarrollo en VSCode:
  * Se creará un fichero .devcontainer por cada servicio. De forma que pueda
desarrollarse sin necesidad de tener las herramientas de desarrollo en la
máquina (Maven y Node).
  * Para acceder a los servicios MySQL, Mongo y RabbitMQ, se creará un dockercompose.yml en el que sólo estén configurados estos servicios y se arrancará
el .devcontainer en modo network=host para que pueda acceder a ellos.
* En la raíz del repositorio deberá incluirse un fichero README.md que describa los
diferentes modos de desarrollo y ejecución de la aplicación.

