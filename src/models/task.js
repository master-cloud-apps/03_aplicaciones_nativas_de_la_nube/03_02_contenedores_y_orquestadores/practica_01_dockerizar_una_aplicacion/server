const mongoose = require('mongoose')

const taskSchema = new mongoose.Schema({
  id: { type: Number, required: true },
  text: { type: String, required: false },
  progress: { type: Number, required: true },
  completed: { type: Boolean, required: true }
})

module.exports = mongoose.model('Task', taskSchema)
