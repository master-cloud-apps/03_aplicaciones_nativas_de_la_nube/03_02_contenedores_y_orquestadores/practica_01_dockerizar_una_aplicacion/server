const { sendMessageToSavedWebSocket } = require('./websocket.js')
const Task = require('./models/task.js')

async function processTask ({ amqpChannel, task }) {
  if (!amqpChannel) {
    console.error('Not connected to RabbitMQ')
  } else {
    const newTask = {
      id: task.id,
      text: task.text
    }

    amqpChannel.sendToQueue('newTasks', Buffer.from(JSON.stringify(newTask)))
  }
}

const consumeTask = ({ amqpChannel }) => {
  amqpChannel.consume('tasksProgress', processTaskProgressMessage, { noAck: true })
}

const processTaskProgressMessage = async msg => {
  const taskMessage = JSON.parse(msg.content.toString())

  console.log('Message received: ' + JSON.stringify(taskMessage))
  sendMessageToSavedWebSocket(taskMessage)
  return Task.findOneAndUpdate(
    { id: taskMessage.id },
    { ...taskMessage, text: taskMessage.result }
  )
}

module.exports = {
  processTask, consumeTask, processTaskProgressMessage
}
